hi clear Normal
set bg&

" Remove all existing highlighting and set the defaults.
hi clear

" Load the syntax highlighting defaults, if it's enabled.
if exists("syntax_on")
	syntax reset
endif

let colors_name = "better_default"

hi Class  ctermfg=6  ctermbg=None  cterm=None
hi Function  ctermfg=6  ctermbg=None  cterm=None
hi Search  ctermfg=3  ctermbg=None  cterm=None
hi Delimiter  ctermfg=11  ctermbg=None  cterm=None
hi Conceal  ctermfg=NONE  ctermbg=None  cterm=None

set fillchars+=vert:\ 
hi LineNr ctermbg=black
hi VertSplit ctermfg=black

hi TabLineFill ctermfg=black ctermbg=black
hi TabLine ctermfg=250 ctermbg=black cterm=none
hi TabLineSel ctermfg=255 ctermbg=black cterm=none
hi StatusLine cterm=italic ctermbg=black ctermfg=black
hi StatusLineNC cterm=none ctermbg=black ctermfg=black

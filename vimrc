if empty(glob('~/.vim/autoload/plug.vim'))
	silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
	\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

call plug#begin()
Plug 'KeitaNakamura/tex-conceal.vim'
Plug 'tpope/vim-surround'
Plug 'michaeljsmith/vim-indent-object'

Plug 'dag/vim-fish'
Plug 'rust-lang/rust.vim'
Plug 'lervag/vimtex'

"Plug 'w0rp/ale'
Plug 'vim-airline/vim-airline'
Plug 'machakann/vim-highlightedyank'

Plug 'junegunn/fzf', { 'do': { -> fzf#install()}}
Plug 'junegunn/fzf.vim'

"coc-rust-analyzer coc-clangd
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()


let g:python_recommended_style = 0

if &compatible
	set nocompatible
endif

set viminfo=%,'50,<100,s10,:100,h,n~/.vim/.viminfo

cnoreabbrev make make -j8


let mapleader = ","

nmap <F7> :tabp<cr>
nmap <F8> :tabn<cr>
"nmap J :tabp<cr>
"nmap K :tabn<cr>
set tabstop=2
set noet sw=2
set nu rnu
colorscheme better_default
set splitbelow
set splitright

nnoremap j gj
nnoremap k gk
"nnoremap <down> gj
"nnoremap <up> gk
"nnoremap j <nop>
"nnoremap k <nop>
"nnoremap h <nop>
"nnoremap l <nop>
nnoremap <down> <nop>
nnoremap <up> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>

map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
map <C-left> <C-w>h
map <C-down> <C-w>j
map <C-up> <C-w>k
map <C-right> <C-w>l

set ignorecase
set smartcase
set incsearch

set wildignore=*.o,*.exe,*.swp
set confirm	" get a dialog when :q, :w, or :wq fails
set hidden	" remember undo after quitting
set autoindent

if !isdirectory($HOME."/.vim/undo-dir")
	call mkdir($HOME."/.vim/undo-dir", "", 0700)
endif
set undodir=~/.vim/undo-dir
set undofile

nmap <C-p> :Files<CR>

so ~/.vim/mine/czechkeys.vim
map <F9> :setlocal spell! spelllang=en<CR>
map <F10> :setlocal spell! spelllang=en,cs<CR>:call ToggleCzechKeys()<CR>
map <F12> :setlocal spell! spelllang=ru<CR>
inoremap <C-l> <c-g>u<Esc>[s1z=`]a<c-g>u

inoremap <C-e> <Esc>/<++><Enter>"_c4l
vnoremap <C-e> <Esc>/<++><Enter>"_c4l
nnoremap <C-e> <Esc>/<++><Enter>"_c4l


nnoremap <C-s> :%s/\([^a-zA-Z0-9_]\)\([^a-zA-Z0-9_]\)/\1\2/g<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>

"This should be moved to its own directory based on Makefile, but this is nice default
let g:ale_cpp_gcc_options = "-std=c++17 -O2 -Wno-unused-result -Wall -Wextra -Wno-unused-parameter -Wcast-align -Wwrite-strings -Wlogical-op -Wredundant-decls"
let g:ale_linters = {
\   'c': ['gcc'],
\   'cpp': ['gcc'],
\}

let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = 0
let g:ale_lint_on_save = 1


set t_Co=256

"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
let g:airline_theme='mine'
let g:airline_skip_empty_sections = 1
let g:airline#extensions#ale#enabled = 1
set laststatus=2



fun! OpenBothFiles( arg ) "{{{
	let parts = split(a:arg, "[.]")

	if len(parts) < 2
		execute "tabe " a:arg
		return
	endif

	let root = parts[0]
	let extension = parts[1]
	if extension == "h"
		let extension = "cpp"
		execute "tabe" root.'.'.extension
		execute "vsplit" a:arg
		execute "wincmd h"
	elseif extension == "cpp"
		let extension = "h"
		execute "tabe " a:arg
		execute "vsplit" root.'.'.extension
		execute "wincmd h"
	else
		execute "tabe " a:arg
	endif
endfunction "}}}

command! -nargs=1 -complete=file Tabe :call OpenBothFiles( '<args>' )
:cabbrev tabe Tabe


if filereadable(".vimrc_local")
	so .vimrc_local
endif






set updatetime=300

if has("patch-8.1.1564")
	" Recently vim can merge signcolumn and number column into one
	set signcolumn=number
else
	set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
	\ pumvisible() ? "\<C-n>" :
	\ <SID>check_back_space() ? "\<TAB>" :
	\ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction


" Use <c-space> to trigger completion.
if has('nvim')
	inoremap <silent><expr> <c-space> coc#refresh()
else
	inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
	if (index(['vim','help'], &filetype) >= 0)
		execute 'h '.expand('<cword>')
	else
		call CocAction('doHover')
	endif
endfunction


" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)


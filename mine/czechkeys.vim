let g:CzechKeysOn=0

function! ToggleCzechKeys()
	if !g:CzechKeysOn
		call CzechKeys()
	else
		call CzechKeysOff()
	endif
endfunction

function! CzechKeys()
	let g:CzechKeysOn=1
	imap 'a á
	imap 'e é
	imap 'i í
	imap 'o ó
	imap 'u ú
	imap 'y ý
	imap a' á
	imap e' é
	imap i' í
	imap o' ó
	imap u' ú
	imap y' ý
	imap 'A Á
	imap 'E É
	imap 'I Í
	imap 'O Ó
	imap 'U Ú
	imap 'Y Ý
	imap A' Á
	imap E' É
	imap I' Í
	imap O' Ó
	imap U' Ú
	imap Y' Ý

	imap uo ů
	imap Uo Ů

	imap >c č
	imap >d ď
	imap >e ě
	imap >n ň
	imap >r ř
	imap >s š
	imap >t ť
	imap >z ž
	imap c< č
	imap d< ď
	imap e< ě
	imap n< ň
	imap r< ř
	imap s< š
	imap t< ť
	imap z< ž

	imap >C Č
	imap >D Ď
	imap >E Ě
	imap >N Ň
	imap >R Ř
	imap >S Š
	imap >T Ť
	imap >Z Ž
	imap C< Č
	imap D< Ď
	imap E< Ě
	imap N< Ň
	imap R< Ř
	imap S< Š
	imap T< Ť
	imap Z< Ž
endfunction

function! CzechKeysOff()
	let g:CzechKeysOn=0
	iunmap 'a
	iunmap 'e
	iunmap 'i
	iunmap 'o
	iunmap 'u
	iunmap 'y
	iunmap a'
	iunmap e'
	iunmap i'
	iunmap o'
	iunmap u'
	iunmap y'
	iunmap 'A
	iunmap 'E
	iunmap 'I
	iunmap 'O
	iunmap 'U
	iunmap 'Y
	iunmap A'
	iunmap E'
	iunmap I'
	iunmap O'
	iunmap U'
	iunmap Y'

	iunmap uo
	iunmap Uo

	iunmap >c
	iunmap >d
	iunmap >e
	iunmap >n
	iunmap >r
	iunmap >s
	iunmap >t
	iunmap >z
	iunmap c<
	iunmap d<
	iunmap e<
	iunmap n<
	iunmap r<
	iunmap s<
	iunmap t<
	iunmap z<

	iunmap >C
	iunmap >D
	iunmap >E
	iunmap >N
	iunmap >R
	iunmap >S
	iunmap >T
	iunmap >Z
	iunmap C<
	iunmap D<
	iunmap E<
	iunmap N<
	iunmap R<
	iunmap S<
	iunmap T<
	iunmap Z<
endfunction

inoremap <F5> <Esc>:w<CR>:!xelatex<space><c-r>%<Enter>a
nnoremap <F5> :w<CR>:!xelatex<space><c-r>%<Enter>
nnoremap <C-F5> :w<CR>:%!vlna -f -s -v KkSsVvZzOoUuAaIi<CR>
nnoremap <S-F5> :!bibtex <c-r>%<backspace><backspace><backspace><backspace><Enter>
map <F4> :!zathura <c-r>%<backspace><backspace><backspace>pdf &<CR><CR>


inoremap -- –

imap ,. \

inoremap ,d \mathrm{d}
inoremap ,p \partial

inoremap ,sav <++>

inoremap ,prevp1    <Esc>?<++><Enter>"_c4l
inoremap ,prevp2    <Esc>?<++><Enter>n"_c4l
inoremap ,prevp3    <Esc>?<++><Enter>nn"_c4l

imap ,cha	\chapter{<++>}<CR><CR><++>,prevp2
imap ,sec	\section{<++>}<CR><CR><++>,prevp2
imap ,ssec	\subsection{<++>}<CR><CR><++>,prevp2
imap ,sssec	\subsubsection{<++>}<CR><CR><++>,prevp2

imap ,lab	\label{}<left>
imap ,ref	\ref{}<left>
imap ,cap	\caption{<++>}<CR>,prevp1


imap ,eq	$<++>$<++>,prevp2

imap ,beq	\begin{equation}<CR><C-i><BS><CR><BS>\end{equation}<up>
imap ,b*eq	\begin{equation*}<CR><C-i><BS><CR><BS>\end{equation*}<up>
imap ,bfig	\begin{figure}[h]<CR><C-i><BS><CR><BS>\end{figure}<up>
imap ,beg	\begin{<++>}<CR><C-i><BS><CR><BS>\end{<++>},prevp3

imap ,frac	\frac{<++>}{<++>}<++>,prevp3
imap ,vec	\vec{<++>}<++>,prevp2
imap ,der	\od{<++>}{<++>}<++>,prevp3

imap ,benum	\begin{enumerate}<CR><C-i><BS><CR><BS>\end{enumerate}<up>
imap ,bitem	\begin{itemize}<CR><C-i><BS><CR><BS>\end{itemize}<up>
imap ,it	\item 

let g:tex_flavor='latex'
let g:vimtex_view_method='zathura'
let g:tex_comment_nospell= 1
set conceallevel=1
let g:tex_conceal="abdgm"

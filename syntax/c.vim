if exists("b:current_syntax")
  finish
endif

syn match cCustomFunc "\w\+\s*(\@="
hi def link cCustomFunc Function

syn match cCustomScope "::"
syn match cCustomClass "\w\+\s*::"
hi def link cCustomClass Function

syn match cDelimiter "[+~*&!%^/<>={}()|.,;\\-\[\]]"
let c_no_bracket_error = 0
hi def link cDelimiter Delimiter

syn match Type "[a-zA-Z_][a-zA-Z0-9_]*\ze\*[ *]*[a-zA-Z_]"
syn match Type "[a-zA-Z_][a-zA-Z0-9_]*\ze&[ &]*[a-zA-Z_]"
syn match Type "[a-zA-Z_][a-zA-Z0-9_]*\ze  *[a-zA-Z_]"

"syn match Number "[^a-zA-Z0-9][0-9][0-9]*[a-zA-Z][a-zA-Z]*"hs=s+1
"syn match Number "[^a-zA-Z0-9][0-9][0-9]*.[0-9][0-9]*[a-zA-Z][a-zA-Z]*"hs=s+1
"syn match Number "[^a-zA-Z0-9].[0-9][0-9]*[a-zA-Z][a-zA-Z]*"hs=s+1


syn keyword Type u8
syn keyword Type u16
syn keyword Type u32
syn keyword Type u64
syn keyword Type u128

syn keyword Type i8
syn keyword Type i16
syn keyword Type i32
syn keyword Type i64
syn keyword Type i128

syn keyword Type f32
syn keyword Type f64
syn keyword Type f128

syn keyword Type Vector
syn keyword Type vec2
syn keyword Type vec3
syn keyword Type vec4
syn keyword Type vec2o3
syn keyword Type vec2i
syn keyword Type vec3i
syn keyword Type vec4i
syn keyword Type mat3
syn keyword Type mat4
syn keyword Type quat
syn keyword Type Color

syn keyword Type uint_t

syn keyword Type MutString

syn keyword Type GLuint
syn keyword Type GLfloat

syn keyword Type vector
syn keyword Type string
